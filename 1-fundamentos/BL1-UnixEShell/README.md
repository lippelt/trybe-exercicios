<!-- # BLOCO 1 - Unix & Bash

Esse bloco pertence ao módulo de `fundamentos` do curso da [Trybe](https://www.betrybe.com/).

No primeiro bloco estudei os principais componentes dos sistemas UNIX, suas variações e aprendi os principais comandos a serem usados no terminal (Bash). -->